package mundo;

import java.util.Date;

public class Clientes extends Trabajo
{
	protected String nombreCliente;
	
	public Clientes(Date pFecha, String pLugar, TipoEventoTrabajo pTipo,
			boolean pObligatorio, boolean pFormal, String pCliente) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		nombreCliente = pCliente;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

		public String toString()
	{
			return "Evento con CLIENTES: \n"
			  		+"FECHA: " + this.fecha
			  		+"\nLUGAR: " + this.lugar
			  		+"\nTIPO EVENTO: " + this.tipo
			  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
			  		+"\nFORMAL: "+ conv(this.formal);
	}
	
}
