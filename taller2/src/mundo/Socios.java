package mundo;

import java.util.Date;

public class Socios extends Trabajo
{	
	protected String nombreSocio;
	
	public Socios(Date pFecha, String pLugar, TipoEventoTrabajo pTipo,
			boolean pObligatorio, boolean pFormal, String pNombre) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		nombreSocio = pNombre;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

		public String toString()
	{
			return "Evento con SOCIOS: \n"
			  		+"FECHA: " + this.fecha
			  		+"\nLUGAR: " + this.lugar
			  		+"\nTIPO EVENTO: " + this.tipo
			  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
			  		+"\nFORMAL: "+ conv(this.formal);
	}

}
